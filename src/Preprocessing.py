# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).


import numpy as np
import torch
import matplotlib.pyplot as plt
import os
import cv2
from tqdm import tqdm
from skimage.transform import swirl
from scipy.ndimage import gaussian_filter
from scipy import ndimage
from scipy.signal import find_peaks
import copy


## Image processing
def mask_devision(mask):
    '''Uses a gaussian to smooth a given mask and compute the edge of the mask'''
    filtered = gaussian_filter(mask, [1.6,1.6])
    max_val = np.max(mask)
    mask_edge = np.zeros_like(mask)
    mask_full = np.zeros_like(mask)
    mask_edge[np.logical_and(filtered>max_val/2, filtered<max_val*4/5)]=max_val
    mask_full[tuple([filtered>max_val/2])]=max_val
    return mask_full, mask_edge

def gauss_Mask(mask):
    '''Same as above, but only the smoothing part'''
    filtered = gaussian_filter(mask, [1.6,1.6])
    max_val = np.max(mask)
    mask_full = np.zeros_like(mask)
    mask_full[tuple([filtered>max_val/2])]=max_val
    return mask_full

## Importing data
def normalize_intensity(im):
    '''Normalizes the intensity of an image to values between 0 and 1'''
    im = 1.0*im
    im = im - np.min(im)
    im = im / np.max(im)
    im = (im+im**3/3)/(1+im+im**3)
    return im/np.max(im)

def normalizer_nothing(im):
    '''only rescales the image to values between 0 and 1'''
    im = 1.0*im
    im = im - np.min(im)
    return im/np.max(im)



## Load MRT Data (including Masks) or Pictures only
def getMrtData(dim, image_path, mask_paths):
    '''Loads images and masks from given paths and resizes them to the given dimension, which should usually be the image dimension'''
    files = list(os.listdir(image_path))
    im_array = []
    mask_array =[]
    N_masks = len(mask_paths)
    for i in tqdm(files): 
        im = cv2.resize(cv2.imread(os.path.join(image_path,i))[:,:,0], (dim, dim))
        masks_i = []
        for k in range(N_masks):
            if os.path.isfile(os.path.join(mask_paths[k],i)):
                mask =  cv2.resize(cv2.imread(os.path.join(mask_paths[k],i))[:,:,0], (dim, dim))
                mask = gauss_Mask(mask)/np.max(mask)
                masks_i.append(mask)
            else:
                masks_i.append(np.zeros((dim,dim)))
        im = im / np.max(im)
        im_array.append(im)
        mask_array.append(np.array(masks_i))
    return im_array, mask_array


def getMrtPictures(dim, image_path, normalizer = normalize_intensity, edge_off = None):
    '''Loads images from a given path and resizes them to the given dimension, which should usually be the image dimension'''
    testing_files = os.listdir(image_path)
    testing_files.sort()
    im_array = []
    for i in tqdm(testing_files): 
        im = cv2.resize(cv2.imread(os.path.join(image_path,i))[:,:,0], (dim,dim))
        if edge_off is not None:
            im = im[edge_off:-edge_off, edge_off:-edge_off]
        
        im = normalizer(im)

        im_array.append(im)
   
    if edge_off is None:
        edge_off=0
    
    tensordata = torch.tensor(np.array(im_array)).float()
    tensordata = tensordata.view(len(im_array),1,dim-2*edge_off,dim-2*edge_off)

    return tensordata

## Following functions build up the genDistortion() function, which takes images with masks and adds a given Number of distorted images with fitting masks to enlarge the trainig set
def v_shear(dim, im, v_pos, v_shift):
    '''vertical shear'''
    upper = cv2.resize(im[0:v_pos], (dim, v_pos-v_shift))
    lower = cv2.resize(im[v_pos:], (dim, dim-v_pos+v_shift))
    return np.append(upper, lower, axis=0)
def h_shear(dim, im, h_pos, h_shift):
    '''horizontal shear'''
    upper = cv2.resize(im[:,0:h_pos], (h_pos-h_shift, dim))
    lower = cv2.resize(im[:,h_pos:], (dim-h_pos+h_shift, dim))
    return np.append(upper, lower, axis=1)

def genShearIm(dim, im, masks):
    '''Performs to vertical and two horizontal shears'''
    v_pos = int(dim/4+np.random.rand()*dim/2)
    v_shift = int(dim/4*np.random.rand() - dim/8)
    N_masks = masks.shape[0]

    im_n = v_shear(dim, im, v_pos, v_shift)
    for k in range(N_masks):
        masks[k] = v_shear(dim, masks[k], v_pos, v_shift)

    v_pos = int(dim/4+np.random.rand()*dim/2)
    v_shift = int(dim/10*np.random.rand() - dim/20)

    im_n = v_shear(dim, im_n, v_pos, v_shift)
    for k in range(N_masks):
        masks[k] = v_shear(dim, masks[k], v_pos, v_shift)

    h_pos = int(dim/6 + 2*dim/3*np.random.rand())
    h_shift = int(dim/6*np.random.rand() - dim/12)

    im_n = h_shear(dim, im_n, h_pos, h_shift)
    for k in range(N_masks):
        masks[k] = h_shear(dim, masks[k], h_pos, h_shift)

    h_pos = int(dim/6 + 2*dim/3*np.random.rand())
    h_shift = int(dim/10*np.random.rand() - dim/20)

    im_n = h_shear(dim, im_n, h_pos, h_shift)
    for k in range(N_masks):
        masks[k] = h_shear(dim, masks[k], h_pos, h_shift)

    return im_n, masks


def genIntensityMap(dim):
    '''Gives an intensity map to modify the images brightness'''
    map = np.ones((dim,dim))
    ## peaks
    for i in range(3):
        center = dim*np.random.rand(2)
        var_1, var_2 = np.random.rand(2)*dim**2
        map += 0.2*np.random.rand()*np.exp(-(np.tile(np.arange(dim),(dim,1))-center[0])**2/var_1 - (np.tile(np.array([np.arange(dim)]).transpose(),(1,dim))-center[1])**2/var_2)
    return map

def genSwirlIm(dim, im, masks):
    '''Combines the beforementioned with an additional swirl method to further distored the image'''
    N_masks = masks.shape[0]
    center=dim*np.random.rand(2)
    strength=(np.random.rand()-0.5)*1.45
    radius=dim*(1+np.random.rand())/2

    center2=dim*np.random.rand(2)
    strength2=(np.random.rand()-0.5)*1.45
    radius2=dim*(1+np.random.rand())/2

    roll_1, roll_2 = (np.random.rand(2)*16-8).astype('int')

    swirl_bild=swirl(im, center=center, rotation=0, strength=strength, radius=radius)
    swirl_bild=swirl(swirl_bild, center=center2, rotation=0, strength=strength2, radius=radius2)
    swirl_bild = np.roll(swirl_bild, roll_1, axis=0)
    swirl_bild = np.roll(swirl_bild, roll_2, axis=1)
    swirl_bild = swirl_bild * genIntensityMap(dim)
    swirl_bild = swirl_bild / np.max(swirl_bild)

    swirl_masks=masks
    for k in range(N_masks):
        swirl_masks[k] = swirl(masks[k].astype('float32'), center=center, rotation=0, strength=strength, radius=radius)
        swirl_masks[k] = swirl(swirl_masks[k], center=center2, rotation=0, strength=strength2, radius=radius2)
        swirl_masks[k] = np.roll(swirl_masks[k], roll_1, axis=0)
        swirl_masks[k] = np.roll(swirl_masks[k], roll_2, axis=1)
        boole=tuple([swirl_masks[k]>0.5])
        swirl_masks[k]=np.zeros_like(masks[k])
        swirl_masks[k][boole]=1
        swirl_masks[k] = gauss_Mask(swirl_masks[k])

    return swirl_bild, swirl_masks


def genDistorsions(dim, images, masks, N_dists, normalizer = normalize_intensity):
    '''Generates N_dists distorsions for every image'''
    im_array = []
    masks_array = []
    for i in tqdm(range(len(images))):
        im_array.append(normalizer(images[i]))
        masks_array.append(masks[i])
        for k in range(N_dists):
            sw_bild, sw_masks = genShearIm(dim, images[i], copy.deepcopy(masks[i]))
            swirl_bild, swirl_masks = genSwirlIm(dim, sw_bild, sw_masks)
            im_array.append(normalizer(swirl_bild))
            masks_array.append(swirl_masks)
    return torch.tensor(np.array(im_array)).float(), torch.tensor(np.array(masks_array)).float()


## Generate Diaphragms from Masks
def find_height_of_dia(mask):
    '''Finds the height of the diaphragm in a given mask'''
    dim = mask.shape[0]
    width = (dim - np.flip(mask,axis=1).argmax(axis=1)) - mask.argmax(axis=1) # width from leftmost to rightmost point
    width[width == dim] = 0
    com = ndimage.center_of_mass(mask) 
    line = int(com[0])# + np.sum(width>0)/4) # upper bound for where to find height_of_dia
    while line < dim and width[line]>0 and width[line] == np.sum(mask[line]):
        line+=1
    if line == dim or width[line]==0:
        #print("Height of dia found at bottom!")
        height_of_dia = int(com[0])# + np.sum(width>0)/4)
    else:
        height_of_dia = line-1
    return height_of_dia

def boundary_peaks(res, height_of_dia, mask):
    '''Finds the boundary peaks of the diaphragm in a given mask'''
    peaks = find_peaks(res, distance = 15, height=height_of_dia)[0]
    dim = mask.shape[0]
    occ = np.sum(mask, axis=0)>0
    left, right = np.argmax(occ)+3,dim - np.argmax(np.flip(occ))-3
    if len(peaks)==0:
        peaks = np.array([left, right])
    else:
        if peaks[0]>left+30:
            peaks = np.concatenate([np.array([left]), peaks])
        if peaks[-1]<right-30:
            peaks = np.concatenate([peaks, np.array([right])])
    return peaks

def find_bottom(mask, height_of_dia):
    '''Finds the bottom of the lung in a given mask'''
    dim = mask.shape[0]
    test = np.flip(mask, axis=0)
    res = dim - test.argmax(axis=0)-1  # correction for index offset
    res[res==dim-1]=0
    peaks = boundary_peaks(res, height_of_dia, mask)
    res[0:peaks[0]]=dim
    res[peaks[-1]:]=dim
    return res, peaks[0], peaks[-1]

def analyze_mask(mask):
    '''Analyzes a given mask and returns the bottom heights and the left and right edge of the diaphragm'''
    height_of_dia = find_height_of_dia(mask)
    bottom_heights, bottom_left, bottom_right = find_bottom(mask, height_of_dia)
    
    return {"Bottom heights":bottom_heights, 'Left dia edge':bottom_left, 'Right dia edge':bottom_right}

def conn_comps(mask):
    '''Finds the largest connected component in a given mask'''
    label_mask, N_labels = ndimage.label(mask)
    sizes = ndimage.sum(mask, label_mask, range(N_labels+1))
    max_ind = sizes.argmax()
    max_val = sizes[max_ind]
    mask_n = (label_mask == max_ind)
    sizes[max_ind]=0
    next_ind = sizes.argmax()
    next_val = sizes[next_ind]
    mask_n2 = (label_mask == next_ind)
    if max_val > 2*next_val: 
        return mask_n
    if ndimage.center_of_mass(mask_n)[1]>ndimage.center_of_mass(mask_n2)[1]:
        return mask_n
    else:
        return mask_n2

def get_dias_analysis(targets):
    '''Returns the analysis of the diaphragms in the given masks'''
    analysis = []
    N_dias = targets.shape[1]
    for frame in range(targets.shape[0]):
        analysis_frame=[]
        for i in range(N_dias):
            analysis_frame.append(analyze_mask(targets[frame][i].numpy()))
        analysis.append(analysis_frame)
    return analysis

def get_1Hot(targets, use_conn_comps=True, check_middlepeak=False):
    '''Selected brightest pixel in each column of the mask and returns a 1-hot encoded mask'''
    N = targets.shape[0]
    dim = targets.shape[2]
    N_dias = targets.shape[1]
    dia_masks = torch.zeros(N,1,dim,dim)
    analysis = get_dias_analysis(targets)
    for frame in range(N):
        for side in range(N_dias):
            dia_mask_side = torch.zeros(dim,dim)
            left = analysis[frame][side]["Left dia edge"]
            right = analysis[frame][side]["Right dia edge"]
            xs = np.arange(left+1, right, 1, dtype=int)
            ys = analysis[frame][side]["Bottom heights"][xs]

            dia_mask_side[ys, xs] = 1
            if use_conn_comps:
                conn_comp = conn_comps(ndimage.convolve(dia_mask_side.numpy(), np.ones((3,3)))>0)
                dia_mask_side = dia_mask_side * conn_comp
                xs = xs[(((dia_mask_side.sum(axis=0))>0).numpy().argmax()-xs[0]):]
                ys_fill = dia_mask_side[:,xs].numpy().argmax(axis=0)
                # smoothed = ndimage.convolve1d(ys_fill, np.ones(7)/7, mode='nearest')
                # ys_fill[ys_fill==0]=smoothed[ys_fill==0]
                if check_middlepeak:
                    peaks = find_peaks(ys_fill, width=3, prominence=3)[0]
                    valid = (peaks>xs[4])*(peaks<xs[-4])
                    if valid.any():
                        co = peaks[valid][0]
                        xs=xs[co:]
                        ys_fill = ys_fill[co:]

                dia_masks[frame, 0, ys_fill, xs] = 1
            else:
                dia_masks[frame, 0] += dia_mask_side
    return dia_masks>0


## Functions to Plot the training data and results
def plotMask(Left,Right):
    '''Plots two images (for example the input image and one mask) side by side.'''
    combined = np.hstack((Left, Right))
    plt.figure(figsize=(25,10))
    plt.imshow(combined)    
    plt.show()

def plotMasksBunch(Left,Right,n_rows=2, save=None, show=True):
    '''Same as above, but for lists of data, plotting n_row rows of 3 example each'''
    sample = []

    for i in range(3*n_rows):
        left = Left[i]
        right = Right[i]
        combined = np.hstack((left,right))
        sample.append(combined)  

    for i in range(0,3*n_rows,3):
        plt.figure(figsize=(25,10))
        plt.subplot(1,3,1)
        plt.imshow(sample[i])
        plt.subplot(1,3,2)
        plt.imshow(sample[i+1])
        plt.subplot(1,3,3)
        plt.imshow(sample[i+2])
        if save is not None:
            plt.savefig(save+'Row{:02d}'.format(i))
        if show:
            plt.show()
        else:
            plt.close()

def plotMaskDias(Left,Right, Dia):
    '''Plots two images (for example the input image and one mask) side by side'''
    combined = np.hstack((Left, Right))
    plt.figure(figsize=(25,10))
    plt.imshow(combined)
    plt.plot(Dia[0], Dia[1], color='red', linewidth=2)
    plt.show()

def plotMasksBunchDias(Left,Right,Dias, n_rows=2):
    '''Same as above, but for lists of data, plotting n_row rows of 3 example each'''
    sample = []
    for i in range(3*n_rows):
        left = Left[i]
        right = Right[i]
        combined = np.hstack((left,right))
        sample.append(combined)  

    for i in range(0,3*n_rows,3):
        plt.figure(figsize=(25,5*n_rows))
        plt.subplot(n_rows,3,1+i)
        plt.imshow(sample[i])
        plt.plot(Dias[i+0][0],Dias[i+0][1], color='red', linewidth=2)
        plt.subplot(n_rows,3,2+i)
        plt.imshow(sample[i+1])
        plt.plot(Dias[i+1][0],Dias[i+1][1], color='red', linewidth=2)
        plt.subplot(n_rows,3,3+i)
        plt.imshow(sample[i+2])
        plt.plot(Dias[i+2][0],Dias[i+2][1], color='red', linewidth=2)
        plt.show()