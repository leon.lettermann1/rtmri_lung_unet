# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).


import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import gc


class UNET(nn.Module):
    '''UNET class for the neural network'''
    def __init__(self, in_channels, out_channels, N_dias, Exp_Padding=1):
        super().__init__()
        self.N_dias = N_dias
        self.out_channels = out_channels
        self.conv1 = self.contract_block(in_channels, 32, 5, 2)
        self.conv2 = self.contract_block(32, 64, 3, 1)
        self.conv3 = self.contract_block(64, 128, 3, 1)

        self.upconv3 = self.expand_block(128, 64, 3, 1, Exp_Padding)
        self.upconv2 = self.expand_block(64*2, 32, 3, 1, Exp_Padding)
        self.upconv1 = self.expand_block(32*2, out_channels+N_dias, 3, 1, 1)

        self.softmax = torch.nn.Sigmoid()

    def __call__(self, x):

        conv1 = self.conv1(x)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        upconv3 = self.upconv3(conv3)
        upconv2 = self.upconv2(torch.cat([upconv3, conv2], 1))
        upconv1 = self.upconv1(torch.cat([upconv2, conv1], 1))
        return upconv1[:,0:self.out_channels], self.softmax(upconv1[:,self.out_channels:self.out_channels+self.N_dias])

    def init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                nn.init.xavier_uniform_(m.weight)

    def contract_block(self, in_channels, out_channels, kernel_size, padding):

        contract = nn.Sequential(
            torch.nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, stride=1, padding=padding, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
            torch.nn.Conv2d(out_channels, out_channels, kernel_size=kernel_size, stride=1, padding=padding, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        )
        return contract

    def expand_block(self, in_channels, out_channels, kernel_size, padding, output_padding):

        expand = nn.Sequential(torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=padding, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
            torch.nn.Conv2d(out_channels, out_channels, kernel_size, stride=1, padding=padding, bias=False),
            torch.nn.BatchNorm2d(out_channels),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(out_channels, out_channels, kernel_size=3, stride=2, padding=1, output_padding=output_padding) 
            )
        return expand


class dataset(Dataset):
    '''Dataset class for the training data'''
    def __init__(self, path, poly_mean=None, poly_std=None):
        self.inputs  = torch.load(path[0], weights_only=False)
        self.targets = torch.load(path[1], weights_only=False)
        self.dias = torch.load(path[2], weights_only=False).type(torch.FloatTensor)

    def __len__(self):
        return self.inputs.shape[0]

    def __getitem__(self, idx):
        return self.inputs[idx], self.targets[idx], self.dias[idx]

def learn_at_rate(unet, dataset_train, dataset_valid, criterion, optimizer, mini_batch_size=13, weight_decay=0, device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")):
    '''Trains the unet with the given data and learning rate until the given number of epochs is reached, or the error on the validation set is smaller than termination_valid_error'''
    N_data=len(dataset_train)
    datal_train = DataLoader(dataset_train, batch_size=mini_batch_size, shuffle=True)
    running_loss_segm, running_loss_pols = 0.0, 0.0
    for x_batch, y_batch, p_batch in datal_train:
        # zero the parameter gradients
        optimizer.zero_grad()
        x_batch = x_batch.to(device)
        y_batch = y_batch.to(device)
        p_batch = p_batch.to(device)
        # forward + backward + optimize
        outputs, poly_out = unet(x_batch)
        loss_segm, loss_pols = criterion(outputs, poly_out, y_batch, p_batch)
        loss = loss_segm + loss_pols
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss_segm += loss_segm.item()
        running_loss_pols += loss_pols.item()
    x_batch = None
    y_batch = None
    p_batch = None
    outputs = None
    running_loss_segm /= np.floor(N_data/mini_batch_size) 
    running_loss_pols /= np.floor(N_data/mini_batch_size)
    valid_loss_here, poly_val_loss = pixel_errors(unet, dataset_valid, eval_batchsize=np.minimum(100, len(dataset_valid)), device=device)
    pixel_loss_here, poly_pix_loss = pixel_errors(unet, dataset_train, samplesize=len(dataset_valid), eval_batchsize=np.minimum(100, len(dataset_valid)), device=device)
    print("Training Loss Segm: %f, Diaphragm: %f; Pixel Error Fraction, Training: %f, Validation: %f; Diaphragm Error Fraction, Training: %f, Validation: %f\n" % (running_loss_segm, running_loss_pols, pixel_loss_here, valid_loss_here, poly_pix_loss, poly_val_loss))
    gc.collect()
    return running_loss_segm, running_loss_pols, pixel_loss_here, valid_loss_here, poly_pix_loss, poly_val_loss

def evaluate(unet, input, batchsize = 1, device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")):
    '''Evaluates the input on the unet, passing batchsize many images at a time to the GPU'''
    m=nn.Softmax2d()
    m.to(device)
    m.eval()
    unet.eval()
    with torch.no_grad():
        output, poly = unet(input[0:batchsize].to(device))
        output = m(output).to('cpu')
        poly = poly.to('cpu')
        for i in range(1, np.floor_divide(input.shape[0], batchsize)):
            output_temp, poly_temp = unet(input[i*batchsize:(i+1)*batchsize].to(device))
            output = torch.cat((output, m(output_temp).to('cpu')))
            poly = torch.cat((poly, poly_temp.to('cpu')))
        i=np.floor_divide(input.shape[0], batchsize)
        if input.shape[0]%batchsize>0:
            output_temp, poly_temp = unet(input[i*batchsize:].to(device))
            output = torch.cat((output, m(output_temp).to('cpu')))
            poly = torch.cat((poly, poly_temp.to('cpu')))
    unet.train()
    return output.detach(), poly.detach()

def comp_segmentation(unet, data, eval_batchsize=1, device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")): # Computes the segmentation of data using the unet, using eval_batchsize many images at a time on the GPU for evaluation
    '''Computes the segmentation of the data using the unet, using eval_batchsize many images at a time on the GPU for evaluation'''
    outputs, polys = evaluate(unet, data, batchsize=eval_batchsize, device=device)
    final_output=(outputs.numpy()>0.5)
    return final_output, polys

# Computes the used validation error, which is not the Training Loss function (a CrossEntropy Loss), but simply the percentage of misclassified pixels
BCELoss = nn.BCELoss()
def pixel_errors(unet, dataset, samplesize=None, eval_batchsize=1, return_details=False, device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")): 
    '''Computes the pixel error (fraction of wrongly classified pixels) of the unet on the dataset, using eval_batchsize many images at a time on the GPU for evaluation'''
    if samplesize is None:
        samplesize=len(dataset)
    perm = np.random.permutation(np.arange(len(dataset)))
    inputs = dataset.inputs[perm[0:samplesize]]
    target = dataset.targets[perm[0:samplesize]]
    dias_target = dataset.dias[perm[0:samplesize]]
    dim = target.shape[1]
    segmentation, dias = comp_segmentation(unet, inputs, eval_batchsize=eval_batchsize, device=device)
    failed = np.sum(((target==1).numpy()!=segmentation[:,1]),axis=(1,2))/dim**2
    if 2 in target:
        failed_2ndSide = np.sum(((target==2).numpy()!=segmentation[:,2]),axis=(1,2))/dim**2
        failed = failed + failed_2ndSide
    pxl_loss = np.mean(failed)
    dia_loss = BCELoss(dias, dias_target).item()
    inputs, target, dias_target, segmentation, dias, failed, perm = None, None, None, None, None, None, None
    return pxl_loss, dia_loss