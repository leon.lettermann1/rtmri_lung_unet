# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).


import numpy as np
from scipy.ndimage import convolve1d
from scipy.optimize import curve_fit

import numpy.polynomial.polynomial as Poly
import pickle

import matplotlib
matplotlib.rcParams['axes.linewidth'] = 3
matplotlib.rcParams['xtick.major.size'] = 7
matplotlib.rcParams['xtick.major.width'] = 3
matplotlib.rcParams['ytick.major.size'] = 7
matplotlib.rcParams['ytick.major.width'] = 3
import matplotlib.pyplot as plt
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams['xtick.labelsize']=24
plt.rcParams['ytick.labelsize']=24
from matplotlib.animation import FuncAnimation, writers

dpis = 70
secs_per_frame = 1/20

def adjust_axis_pos(ax, dx0=0,dx1=0,dy0=0,dy1=0):
    '''Adjust position of axis by dx0,dx1,dy0,dy1'''
    pos = ax.get_position()
    ax.set_position([pos.x0+dx0,pos.y0+dy0,pos.width+dx1,pos.height+dy1])

def plotAnalysis_top(X, masks, plot_inst, plot_inst_masks, m_type, axis_p=None, show=False, plot_elements=None):
    '''Plots a frame of the finished segmentation, detailed views of the segmentation'''
    if axis_p:
        axis = axis_p
    else: 
        fig, axis = plt.subplots(figsize=(15,14), dpi=dpis)
    dim = X.shape[0]
    images_t = np.hstack((X, X))
    images = np.vstack((images_t, np.hstack((np.zeros_like(X),X))))

    if m_type=='Sagittal':
        mask_full_s,mask_edge_s = masks
        masks_full = np.vstack((np.zeros_like(images_t),np.hstack((mask_full_s, np.zeros_like(X)))))
        masks_edge = np.vstack((np.zeros_like(images_t),np.hstack((mask_edge_s, np.zeros_like(X)))))
        mask_overlay=np.vstack((np.hstack((np.zeros_like(X),mask_full_s)),np.zeros_like(images_t)))
        edge_overlay=np.vstack((np.hstack((np.zeros_like(X),mask_edge_s)),np.zeros_like(images_t)))
    elif m_type=='Coronar':
        mask_full_l,mask_full_r,mask_edge_l,mask_edge_r = masks
        masks_full = np.vstack((np.zeros_like(images_t),np.hstack((mask_full_l+mask_full_r, np.zeros_like(X)))))
        masks_edge = np.vstack((np.zeros_like(images_t),np.hstack((mask_edge_l+mask_edge_r, np.zeros_like(X)))))
        mask_overlay=np.vstack((np.hstack((np.zeros_like(X),np.maximum(mask_full_l, mask_full_r))),np.zeros_like(images_t)))
        edge_overlay=np.vstack((np.hstack((np.zeros_like(X),np.maximum(mask_edge_l, mask_edge_r))),np.zeros_like(images_t)))


    masks_full = np.ma.masked_where(masks_full < 0.5, masks_full)
    masks_edge = np.ma.masked_where(masks_edge < 0.5, masks_edge)
    mask_overlay = np.ma.masked_where(mask_overlay < 0.5, mask_overlay)
    edge_overlay = np.ma.masked_where(edge_overlay < 0.5, edge_overlay)

    points_to_plot = plot_inst['points']
    lines_to_plot = plot_inst['lines']

    if plot_elements is None:
        im1 = axis.imshow(images, cmap="bone")
        im2 = axis.imshow(masks_full*180, cmap="nipy_spectral", vmin=0, vmax=255)
        im3 = axis.imshow(masks_edge*225, interpolation = "nearest", resample = False, cmap="nipy_spectral", vmin=0, vmax=255)
        im4 = axis.imshow(mask_overlay*180, cmap="nipy_spectral", vmin=0, vmax=255, alpha=0.4)
        im5 = axis.imshow(edge_overlay*225, interpolation = "nearest", resample = False, cmap="nipy_spectral", vmin=0, vmax=255, alpha = 0.6)
        axis.axis('off')
        im_points, im_lines, mask_points, mask_lines = [],[],[],[]
        for point in points_to_plot:         
            im_points.append(axis.scatter([point[0][0]+dim],[point[0][1]+dim],**point[1]))
        for line in lines_to_plot:
            im_lines.append(axis.plot(np.array(line[0][0])+dim, np.array(line[0][1])+dim, **line[1])[0])

        if plot_inst_masks=={}:
            pass
        else:
            points_to_plot = plot_inst_masks['points']
            lines_to_plot = plot_inst_masks['lines']
            for point in points_to_plot:         
                mask_points.append(axis.scatter([point[0][0]+dim],[point[0][1]],**point[1]))
            for line in lines_to_plot:
                mask_lines.append(axis.plot(np.array(line[0][0])+dim, np.array(line[0][1]), **line[1])[0])
    
    else:
        im1, im2, im3, im4, im5, im_points, im_lines, mask_points, mask_lines = plot_elements
        im1.set_data(images)
        im2.set_data(masks_full*180)
        im3.set_data(masks_edge*225)
        im4.set_data(mask_overlay*180)
        im5.set_data(edge_overlay*225)
        for i, point in enumerate(points_to_plot):         
            if i<len(im_points):
                im_points[i].set_offsets(np.array([[point[0][0]+dim,point[0][1]+dim]]))
        for i, line in enumerate(lines_to_plot):
            if i<len(im_lines):
                im_lines[i].set_data(np.array(line[0][0])+dim, np.array(line[0][1])+dim)

        if plot_inst_masks=={}:
            pass
        else:
            points_to_plot = plot_inst_masks['points']
            lines_to_plot = plot_inst_masks['lines']
            for i, point in enumerate(points_to_plot):         
                if i<len(mask_points):
                    mask_points[i].set_offsets(np.array([[point[0][0]+dim,point[0][1]]]))
            for i, line in enumerate(lines_to_plot):
                if i<len(mask_lines):
                    mask_lines[i].set_data(np.array(line[0][0])+dim, np.array(line[0][1]))

    if show:
        plt.show()
    return im1, im2, im3, im4, im5, im_points, im_lines, mask_points, mask_lines

def plotAnalysis_top_wo_seg(X, masks, plot_inst, plot_inst_masks, m_type, axis_p=None, show=False, alpha_mask=0.1, alpha_edge=0.3):
    '''Plots a frame of the finished analysis'''
    if axis_p:
        axis = axis_p
    else: 
        fig, axis = plt.subplots(figsize=(15,14), dpi=dpis)

    if m_type=='Sagittal':
        mask_full_s,mask_edge_s = masks
        mask_overlay = mask_full_s
        edge_overlay = mask_edge_s
        mask_overlay = np.ma.masked_where(mask_overlay < 0.5, mask_overlay)
        edge_overlay = np.ma.masked_where(edge_overlay < 0.5, edge_overlay)
    elif m_type=='Coronar':
        mask_full_l,mask_full_r,mask_edge_l,mask_edge_r = masks
        mask_overlay = np.maximum(mask_full_l, mask_full_r)
        edge_overlay = np.maximum(mask_edge_l, mask_edge_r)
        mask_overlay = np.ma.masked_where(mask_overlay < 0.5, mask_overlay)
        edge_overlay = np.ma.masked_where(edge_overlay < 0.5, edge_overlay)

    axis.imshow(X, cmap="bone")
    axis.imshow(mask_overlay*180, cmap="nipy_spectral", vmin=0, vmax=255, alpha=alpha_mask)
    axis.imshow(edge_overlay*225, interpolation = "nearest", resample = False, cmap="nipy_spectral", vmin=0, vmax=255, alpha = alpha_edge)
    axis.axis('off')
    points_to_plot = plot_inst['points']
    lines_to_plot = plot_inst['lines']
    for point in points_to_plot:         
        axis.scatter([point[0][0]],[point[0][1]],**point[1])
    for line in lines_to_plot:
        axis.plot(np.array(line[0][0]), np.array(line[0][1]), **line[1])

    if plot_inst_masks=={}:
        pass
    else:
        points_to_plot = plot_inst_masks['points']
        lines_to_plot = plot_inst_masks['lines']
        for point in points_to_plot:         
            axis.scatter([point[0][0]],[point[0][1]],**point[1])
        for line in lines_to_plot:
            axis.plot(np.array(line[0][0]), np.array(line[0][1]), **line[1])

    if show:
        plt.show()

def plotAnalysis(X, masks, analysis, dataset, frame, dt_segm = True, show=True, plot_elements=None, moving_points=None, cursors=None):
    '''Plots a frame of the finished analysis'''
    plot_shape, plots, plot_inst, plot_inst_masks, sin_params, m_type = dataset
    rows = len(plot_shape)
    fig = plt.figure(figsize=(26,13), dpi=dpis)
    ax1 = plt.subplot2grid((1,2), (0,0))
    if dt_segm:
        if plot_elements is None:
            plot_elements = plotAnalysis_top(X, masks, plot_inst[frame], plot_inst_masks[frame], m_type, ax1)
        else:
            plot_elements = plotAnalysis_top(X, masks, plot_inst[frame], plot_inst_masks[frame], m_type, ax1, plot_elements=plot_elements)
    axes = []

    for line in range(rows):
        columns = plot_shape[line]
        for col in range(columns):
            axes.append(fig.add_subplot(rows, 2*columns, line*2*columns + columns + 1 + col))


    fig.subplots_adjust(left=0.02, bottom=0.07, right=1, top=0.96, wspace=None, hspace=None)
    d = 0.035
    dy = 0.02
    dx = 0.027
    dxshift = 0.03
    adjust_axis_pos(ax1, dx0=-d, dx1=2*d, dy0=-d+0.003, dy1=d)
    adjust_axis_pos(axes[0], dx0=-0.02+dxshift, dy0=dy, dy1=-dy, dx1=-dx)
    adjust_axis_pos(axes[1], dy0=dy, dy1=-dy, dx0=-dx+dxshift, dx1=-dx)
    adjust_axis_pos(axes[-1], dx0=-0.042+dxshift, dx1=0.042-2*dx, dy0=2*dy, dy1=-dy)

    if moving_points is None:
        moving_points = []
        for plot_num in range(len(axes)):
            dataset, other = plots[plot_num]
            data, instPlot, instPoints = dataset
            lines = axes[plot_num].plot(secs_per_frame*np.arange(len(data)), data, **instPlot)
            point = axes[plot_num].scatter(secs_per_frame*frame*np.ones(len(data[frame])), [data[frame]], **instPoints)
            moving_points.append(point)
            if 'xlabel' in other:
                axes[plot_num].set_xlabel(other['xlabel'][0], **other['xlabel'][1])
            if 'ylabel' in other:
                axes[plot_num].set_ylabel(other['ylabel'][0], **other['ylabel'][1])
            if 'ylim' in other:
                axes[plot_num].set_ylim(other['ylim'])
            if 'manual colors' in other:
                for num_line in range(len(lines)):
                    lines[num_line].set_color(other['manual colors'][num_line])
            if 'manual linestyles' in other:
                for num_line in range(len(lines)):
                    lines[num_line].set_linestyle(other['manual linestyles'][num_line])
            if 'manual linewidth' in other:
                for num_line in range(len(lines)):
                    lines[num_line].set_linewidth(other['manual linewidth'][num_line])
            if 'label' in other:
                axes[plot_num].legend(other['label'][0], **other['label'][1])
            axes[plot_num].tick_params(axis='both', labelsize= 22)
        #plt.tight_layout()
    else:
        for plot_num in range(len(axes)):
            data = plots[plot_num][0][0]
            moving_points[plot_num].set_offsets(np.stack((secs_per_frame*frame*np.ones(len(data[frame])), data[frame]), axis=1))

    if cursors is None:
        cursors = []
        for ax in axes:
            cursors.append(ax.axvline(x=secs_per_frame*frame, color='k', linestyle='-', linewidth=4.5, alpha=0.7, zorder=-20))
    else:
        for cursor in cursors:
            cursor.set_xdata(secs_per_frame*frame)

    if show:
        plt.show()
    return fig, plot_elements, moving_points, cursors


def animateAnalysis_mp4(inputs, masks, analysis, data, path, dt_segm, first_only=False):
    '''Animates the analysis of the input data'''
    fig, plot_elements, moving_points, cursors = plotAnalysis(inputs[0,0].detach().numpy(), masks[0], analysis[0], data, 0, show=False, dt_segm = dt_segm)
    def func(i): 
        return plotAnalysis(inputs[i,0].detach().numpy(), masks[i], analysis[i], data, i, show=False, 
                                  dt_segm = dt_segm, plot_elements=plot_elements, moving_points=moving_points, cursors=cursors)
    def init():
        return plot_elements, moving_points, cursors
    
    toplet, botlet, sidlets = 0.93, 0.47, [0.018, 0.245, 0.485, 0.745]
    for i, s in enumerate(sidlets):
        fig.text(s, toplet, 'abef'[i], fontsize=36, fontweight='bold', color=['white', 'white', 'black', 'black'][i])
        fig.text(s, botlet, 'cdg '[i], fontsize=36, fontweight='bold', color=['white', 'white', 'black', 'black'][i])

    def animate(i):
        fig, plot_elements, moving_points, cursors = func(i)
        plt.close()
        return plot_elements, moving_points, cursors
    

    anim = FuncAnimation(fig, animate, init_func=init, frames=2 if first_only else inputs.shape[0], interval=secs_per_frame*1000, blit=False)

    FFMpegWriter = writers['ffmpeg']
    writer = FFMpegWriter(fps=1/secs_per_frame, bitrate=-1, extra_args=['-vcodec', 'libx264'])
    anim.save(path, writer=writer, dpi=dpis)

def load_patient(res_base_path, pat, measurement):
    '''Loads the results of a patient previously saved'''
    with open(res_base_path+pat+'/'+measurement+'.pickle', 'rb') as fp:
        results_Read = pickle.load(fp)
    return results_Read

def animation_from_data(res_base_path, pat, measurement, result_path):
    '''Creates an animation from the saved analysis data of a patient'''
    pat_dict = load_patient(res_base_path, pat, measurement)
    animateAnalysis_mp4(pat_dict['Images'], pat_dict['Masks'], pat_dict['Images'], pat_dict['Dataset'], result_path, dt_segm = True)


# Functions for per frame analysis
def do_per_frame(N_frames, func, args):
    '''Runs a function for each frame of the data'''
    data = []
    plot_inst = []
    pass_from_prev = {}
    for frame in range(N_frames):
        data_frame, plot_inst_frame, pass_from_prev = func(frame, *args, **pass_from_prev)
        data.append(data_frame)
        plot_inst.append(plot_inst_frame)
    return data, plot_inst

def af_width_variable(frame, analysis, dias):
    '''Calculates the width of lung at different heights'''
    mean_ext = 1
    measurement_heights = [1/3, 2/3, 0.975]
    data_frame = []
    lines_frame = []
    color_counter=0
    for side in range(len(analysis[1])):
        top_height = analysis[frame][side]['P top max'][1]
        bottom = np.nanmin(dias[frame,0,1][np.arange(analysis[frame][side]['Left dia edge top'], analysis[frame][side]['Right dia edge top'])])
        distance = bottom-top_height
        for q in measurement_heights:
            height = top_height + int(q*distance)
            width = np.mean(analysis[frame][side]['Width'][height-mean_ext:height+mean_ext])
            data_frame.append(width)
            x = np.mean(analysis[frame][side]['X of height'][height])
            lines_frame.append((([x,x+width],[height, height]),{'c':"C{}".format(color_counter), 'linewidth':2}))
            color_counter+=1
    return data_frame, {'points':[], 'lines':lines_frame}, {}

def af_width_fixed(frame, analysis, dias, top_height, distance):
    '''Calculates the width of lung at fixed heights'''
    mean_ext = 1
    measurement_heights = [1/3, 2/3, 0.975]
    data_frame = []
    lines_frame = []
    color_counter=0
    for side in range(len(analysis[1])):
        for q in measurement_heights:
            height = top_height[side] + int(q*distance[side])
            width = np.mean(analysis[frame][side]['Width'][height-mean_ext:height+mean_ext])
            data_frame.append(width)
            x = np.mean(analysis[frame][side]['X of height'][height])
            lines_frame.append((([x,x+width],[height, height]),{'c':"C{}".format(color_counter), 'linewidth':2}))
            color_counter+=1
    return data_frame, {'points':[], 'lines':lines_frame}, {}

def af_width_mean(frame, analysis, dias, top_height, distance):
    '''Calculates the mean width of lung over fixed heights'''
    data_frame = []
    lines_frame = []
    color_counter=8
    for side in range(len(analysis[1])):
        height_up = top_height[side] + int(1/3*distance[side])
        height_down = top_height[side] + int(0.97*distance[side])
        width = np.mean(analysis[frame][side]['Width'][height_up:height_down])
        data_frame.append(width)
        line_front_x = analysis[frame][side]['X of height'][height_up:height_down+1]
        line_front_z = np.linspace(height_up, height_down, len(line_front_x))
        line_back_x = np.flip(line_front_x+analysis[frame][side]['Width'][height_up:height_down+1])
        line = (np.concatenate((line_front_x, line_back_x, line_front_x[:1])), np.concatenate((line_front_z, np.flip(line_front_z), line_front_z[:1])))
        lines_frame.append((line,{'c':"C{}".format(color_counter), 'linewidth':5, 'alpha':0.4}))
        color_counter+=1
    return data_frame, {'points':[], 'lines':lines_frame}, {}

def af_polynomial_movement(frame, analysis, dias, integral_current=np.zeros(2), last_polynom=[np.zeros(5), np.zeros(5)], 
                           last_offset = [0.0,0.0], last_xs = [np.zeros(2),np.zeros(2)]):
    '''Fits a polynomial to the diaphragm and calculates the maximum and mean height of the diaphragm as well as the area swept by the line.'''
    degree=4
    data_frame = []
    plot_inst_frame={'points':[],'lines':[]}
    current_poly = [0,0]
    current_offset = [0,0]
    current_xs = [0,0]
    current_ys = [0,0]
    integral = integral_current
    N_sides = len(analysis[0])
    for side in range(N_sides): # left and right
        # Polyfit
        left = analysis[frame][side]["Left dia edge"]
        right = analysis[frame][side]["Right dia edge"]
        xs_segm = np.arange(left, right, 1, dtype=int)
        ys_segm = analysis[frame][side]["Bottom heights"][xs_segm]
        xs=[]
        ys=[]
        for x,y in zip(xs_segm, ys_segm):
            indxs = np.where(dias[frame, 0, 0]==x)[0]
            if len(indxs)==1:
                if np.abs(y - dias[frame, 0, 1][indxs[0]])<3:
                    xs.append(x)
                    ys.append(y)
        xs = np.array(xs)
        ys = np.array(ys)
        if len(xs)<2:
            data_frame.append(np.nan)
            data_frame.append(np.nan)
            data_frame.append(np.nan)
            current_poly[side], current_offset[side], current_xs[side] = last_polynom[side], last_offset[side], last_xs[side]
            current_ys[side] = np.ones(1).astype(float)*np.nan
            continue
        current_ys[side] = ys
        com_x = int(np.nan_to_num(np.nanmean(xs)))
        xs_shifted = xs - com_x
        p = Poly.polyfit(xs_shifted,ys,degree)
        current_poly[side] = p
        current_offset[side] = com_x
        current_xs[side] = xs
        solution = Poly.polyval(xs_shifted, p)
        max_val = np.min(solution)
        mean_height = np.mean(solution)
        if frame > 0:
            p_f = Poly.polyint(p)
            p_i = Poly.polyint(last_polynom[side])
            indices = [0,-1]
            offset = com_x
            offset_i = last_offset[side]
            x_f, x_i = xs_shifted[indices], last_xs[side][indices]-offset_i
            y_f, y_i = Poly.polyval(x_f, p), Poly.polyval(x_i , last_polynom[side])
            int_current = Poly.polyval(x_f[1], p_f) - Poly.polyval(x_f[0], p_f) - (Poly.polyval(x_i[1], p_i) - Poly.polyval(x_i[0], p_i)) 
            int_boundaries = - (x_i[0]+offset_i-x_f[0]-offset)*(y_f[0]+y_i[0])/2 - (x_f[1]+offset-x_i[1]-offset_i)*(y_f[1]+y_i[1])/2
            int_current += int_boundaries
            integral[side] += int_current
        data_frame.append(max_val)
        data_frame.append(mean_height)
        data_frame.append(integral[side])
    
    analysis_return = {'data':np.array(data_frame),'coeffs': current_poly, 'offset': current_offset, 'xs': current_xs, 'ys':current_ys}
    integra_and_last_polynom = {'integral_current':integral, 'last_polynom':current_poly, 'last_offset':current_offset, 'last_xs':current_xs}
    return analysis_return, plot_inst_frame, integra_and_last_polynom


def af_polynomial_movement_vd(frame, analysis, dias, front_back, integral_current=np.zeros(2), 
                              last_polynom=[np.zeros(1), np.zeros(1)], last_offset = [0.0,0.0], last_xs = [np.zeros(2),np.zeros(2)]):
    '''Calculates a polynomial as bevor, but for the ventrodorsal thorax wall'''
    degree=4
    data_frame = []
    plot_inst_frame={'points':[],'lines':[]}
    current_poly = [0,0]
    current_offset = [0,0]
    current_xs = [0,0]
    integral = integral_current
    for side in range(len(analysis[0])):
        # Polyfit
        x_o_h = np.array(analysis[frame][0]['X of height']) + front_back*np.array(analysis[frame][0]['Width'])
        top_y = (x_o_h>0).argmax()
        x_o_h = x_o_h[x_o_h>0]
        bot_y = (np.abs(x_o_h[10:]-x_o_h[9:-1])>6).argmax()+8
        if bot_y ==8:
            bot_y = len(x_o_h)-2
        x_o_h = x_o_h[0:bot_y]
        hs = np.arange(top_y, top_y + bot_y)
        xs = hs
        ys = x_o_h
        com_x = int(np.nan_to_num(np.nanmean(xs)))
        xs_shifted = xs - com_x
        p = Poly.polyfit(xs_shifted,ys,degree)
        current_poly[side] = p
        current_offset[side] = com_x
        current_xs[side] = xs
        solution = Poly.polyval(xs_shifted, p)
        if front_back == 0:
            max_val = np.min(solution)
            max_x = xs[np.argmin(solution)]
        else:
            max_val = np.max(solution)
            max_x = xs[np.argmax(solution)]
        mean_height = np.mean(solution)
        if frame > 0:
            p_f = Poly.polyint(p)
            p_i = Poly.polyint(last_polynom[side])
            indices = [0,-1]
            offset = com_x
            offset_i = last_offset[side]
            x_f, x_i = xs_shifted[indices], last_xs[side][indices]-offset_i
            y_f, y_i = Poly.polyval(x_f, p), Poly.polyval(x_i , last_polynom[side])
            int_current = Poly.polyval(x_f[1], p_f) - Poly.polyval(x_f[0], p_f) - (Poly.polyval(x_i[1], p_i) - Poly.polyval(x_i[0], p_i)) 
            int_boundaries = - (x_i[0]+offset_i-x_f[0]-offset)*(y_f[0]+y_i[0])/2 - (x_f[1]+offset-x_i[1]-offset_i)*(y_f[1]+y_i[1])/2
            int_current += int_boundaries
            integral[side] += int_current

        plot_inst_frame['lines'].append(((Poly.polyval(xs_shifted, p), xs),{'color':'red', 'linewidth':2}))
        plot_inst_frame['lines'].append(((np.ones_like(xs)*mean_height, xs),{'color':'tab:cyan', 'linewidth':2, 'alpha':0.7}))
        plot_inst_frame['points'].append(((max_val, max_x),{'color':'green', 's':40}))

        data_frame.append(max_val)
        data_frame.append(mean_height)
        data_frame.append(integral[side])
    
    return np.array(data_frame), plot_inst_frame, {'integral_current':integral, 'last_polynom':current_poly, 'last_offset':current_offset, 'last_xs':current_xs}

def af_dia_movement_cor(frame, analysis, dias):
    '''Analyzes the diaphragm movement in coronar view'''
    data_frame = []
    plot_inst_frame={'points':[],'lines':[]}
    positions = []
    color_counter = 0
    for side in range(len(analysis[0])):
        left = analysis[frame][side]['Left dia edge top']
        right = analysis[frame][side]['Right dia edge top']
        if left<120:
            left-=7
        if right >170:
            right-=7
        positions = positions + [left, right]
        xs = dias[frame, 0, 0]
        xs = xs[xs<=right]
        xs = xs[xs>=left]
        ys = dias[frame, 0, 1][xs.astype(int)]
        data_frame.append(np.nanmean(ys))
        plot_inst_frame['lines'].append(((xs, ys), {'linewidth':5, 'c':"C{}".format(color_counter)}))
        color_counter+=1
    positions.sort()
    for left, right in [(positions[1], positions[2]), (positions[0], positions[3])]:
        xs = dias[frame, 0, 0]
        xs = xs[xs<=right]
        xs = xs[xs>=left]
        ys = dias[frame, 0, 1][xs.astype(int)]
        data_frame.append(np.nanmean(ys))
        plot_inst_frame['lines'].append(((xs, ys), {'linewidth':3, 'c':"C{}".format(color_counter)}))
        color_counter+=1
    plot_inst_frame['lines'][3] = (plot_inst_frame['lines'][3][0], {'linewidth':0.5, 'c':'C3'})
    return np.array(data_frame), plot_inst_frame, {}

def af_dia_movement_sag(frame, analysis, dias):
    '''Analyzes the diaphragm movement in sagittal view'''
    data_frame = []
    plot_inst_frame={'points':[],'lines':[]}
    color_counter = 0
    left = analysis[frame][0]['Left dia edge top']
    right = analysis[frame][0]['Right dia edge top']
    xs = dias[frame, 0, 0]
    xs = xs[xs<=right]
    xs = xs[xs>=left]
    ys = dias[frame, 0, 1][xs.astype(int)]
    data_frame.append(np.nanmean(ys))
    plot_inst_frame['lines'].append(((xs, ys), {'linewidth':3, 'c':"C{}".format(color_counter)}))
    return np.array(data_frame), plot_inst_frame, {}

def fit_sin(x, a, w, p):
    return a * np.sin(w*x + p)

def find_periodicity(plot):
    '''Finds the periodicity of a plot'''
    plot_woNaN = plot
    nans = np.isnan(plot)
    plot_woNaN[nans] = np.interp(np.flatnonzero(nans), np.flatnonzero(~nans), plot_woNaN[~nans])
    kernel = np.exp(-np.linspace(-14,14)**2/(2*5**2))
    kernel = kernel/np.sum(kernel)
    gradients = np.gradient(convolve1d(plot_woNaN, kernel, mode='nearest'))
    idx_sign_change = np.where(np.sign(gradients[:-1]) != np.sign(gradients[1:]))[0]
    if len(idx_sign_change)>1:
        period = np.mean(np.diff(idx_sign_change))*2*secs_per_frame
    elif len(idx_sign_change)==1:
        period = np.maximum(idx_sign_change[0], len(plot)-idx_sign_change[0])*2*secs_per_frame
    else:
        period = 100
    return period

def fit_sin_to_plot(plot_in): # plot is frames
    '''Fits a sin function to a plot'''
    plot = plot_in-np.nanmean(plot_in, axis=0)
    N_frames = plot.shape[0]
    xs = np.arange(N_frames)*secs_per_frame
    guess_w = 2*np.pi/find_periodicity(plot)
    guess_a = (np.max(plot)-np.min(plot))/2
    p0 = np.array([guess_a, guess_w, np.nan_to_num(plot[0])/guess_a])
    bounds = (np.array([0, guess_w-0.1, -np.inf]),np.array([np.inf, guess_w+0.1, np.inf]))
    valid = ~np.isnan(plot)
    params = curve_fit(fit_sin, xs[valid], plot[valid], p0=p0, maxfev=5000, bounds=bounds)[0]
    # if np.abs(params[1]-guess_w)>0.5: print("Probably wrong period? Guess: {}, Fit: {}".format(guess_w, params[1]))
    return params