# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).


import numpy as np
from scipy import ndimage
from scipy.signal import find_peaks

from Preprocessing import getMrtPictures, mask_devision
from NeuralNetwork import comp_segmentation

CUPY=False
if CUPY:
    import cupy as cp
    from cupyx.scipy.ndimage import convolve as convolve_gpu



def smooth_segmentation(segmentation, kernel = np.exp(-np.arange(-5,5)**2/4**2)):
    '''Smoothing the segmentation masks with a gaussian kernel'''
    kernel_norm = kernel/np.sum(kernel)
    segm_conv = ndimage.filters.convolve1d(1.0*segmentation, kernel_norm, axis=0, mode='nearest')
    return segm_conv >0.5

def smooth_dia_masks(dias, kernel = np.exp(-np.arange(-5,5)**2/4**2)):
    '''Smoothing the diaphragm masks with a gaussian kernel'''
    kernel_norm = kernel/np.sum(kernel)
    dias_conv = ndimage.filters.convolve1d(1.0*dias, kernel_norm, axis=0, mode='nearest')
    return dias_conv >0.5

def smooth_dias(dias, kernel = np.exp(-np.arange(-5,5)**2/4**2)):
    '''Smoothing the diaphragm height function and set columns without diaphragm to nan'''
    kernel_norm = kernel/np.sum(kernel)
    dias_conv = ndimage.filters.convolve1d(np.nan_to_num(dias[:,:,1]), kernel_norm, axis=0, mode='nearest')
    x_count = ndimage.filters.convolve1d(1.0*(~np.isnan(dias[:,:,1])), kernel_norm, axis=0, mode='nearest') 
    dias_conv[dias_conv>0] = dias_conv[dias_conv>0]/x_count[dias_conv>0]
    dias_conv[dias_conv==0] = np.nan
    return np.concatenate([dias[:,:,0:1], dias_conv.reshape(dias.shape[0], dias.shape[1],1,dias.shape[3])], axis=2)

def conn_comps(mask):
    '''Find the largest connected component in a binary mask'''
    label_mask, N_labels = ndimage.label(mask)
    sizes = ndimage.sum(mask, label_mask, range(N_labels+1))
    mask_n = (label_mask==sizes.argmax())
    mask_n = ndimage.morphology.binary_fill_holes(mask_n)
    return mask_n

def find_width(mask):
    '''Find the width of the lung at each height and the height of the diaphragm'''
    dim = mask.shape[0]
    width = (dim - np.flip(mask,axis=1).argmax(axis=1)) - mask.argmax(axis=1) # width from leftmost to rightmost point
    width[width == dim] = 0
    x_of_height = []  # gives the x cordinate, at which the lung begins at height h
    for line in range(dim):
        x_of_height.append(np.argmax(mask[line]))
    com = ndimage.center_of_mass(mask) 
    line = int(com[0] + np.sum(width>0)/4) # upper bound for where to find height_of_dia
    while line < dim and width[line]>0 and width[line] == np.sum(mask[line]):
        line+=1
    if line == dim or width[line]==0:
        #print("Height of dia found at bottom!")
        height_of_dia = int(com[0] + np.sum(width>0)/4)
    else:
        height_of_dia = line
    return width, x_of_height, height_of_dia

def boundary_peaks(res, height_of_dia, mask):
    '''Find the two peaks of the diaphragm, which are the boundaries of the diaphragm'''
    peaks = find_peaks(res, distance = 15, height=height_of_dia)[0]
    dim = mask.shape[0]
    com1 = ndimage.center_of_mass(mask)
    com = ndimage.center_of_mass(mask[int(com1[0]):])
    line = mask[int(com[0]+com1[0])]
    left = line.argmax()+5
    right = dim - np.flip(line).argmax()-5
    error = 0 

    index_to_delete = [] #collecting peaks to close to com to be diaphragm boundaries
    for index, p in enumerate(peaks):
        if np.abs(p-com[1])<10:
            index_to_delete.append(index)
            if index_to_delete!=[1]:
                print("Suspicious peak (Peaks: {}) at {} removed, as near to com at {}.".format(peaks, index_to_delete, com[1]))
                error = 1
    peaks = np.delete(peaks, index_to_delete)
    if peaks.size<2:
        #print("Not two peaks, {} peaks! PEaks bei {}, height of dia is {}".format(peaks.size, peaks, height_of_dia))
        if peaks.size==1: 
            if peaks[0] > com[1]:
                peaks = np.array([left, peaks[0]])
            elif peaks[0] < com[1]:
                peaks = np.array([peaks[0], right])
            else:
                peaks = np.array([left, right])
                print("Peaks are weird, chose width at height_of_dia instead")
                error = 1
        else:
            peaks = np.array([left, right])
    if error == 1:
        print("Peaks not easily find, now Peaks are {}".format(peaks))
    return peaks

def find_bottom(mask, height_of_dia):
    '''Find the bottom of the diaphragm and the two edges of the diaphragm'''
    dim = mask.shape[0]
    test = np.flip(mask, axis=0)
    res = dim - test.argmax(axis=0)-1  # correction for index offset
    res[res==dim-1]=dim
    maxval=np.min(res)
    left = np.argmax(res>0)
    right = dim - np.argmax(np.flip(res)>0)
    return res, int(np.rint(ndimage.center_of_mass(res==maxval))[0]), left, right


def find_top(mask):
    '''Find the top of the diaphragm and the two edges of the diaphragm'''
    dim = mask.shape[0]
    res = mask.argmax(axis=0)
    res[res==0] = dim
    maxval = np.min(res)
    return res, int(np.rint(ndimage.center_of_mass(res==maxval))[0]), (res<dim).argmax(), dim - (np.flip(res)<dim).argmax()

def analyze_mask(mask):
    '''Analyze the mask and return the relevant information'''
    com = ndimage.center_of_mass(mask)
    width, x_of_height, height_of_dia = find_width(mask)
    top_heights, top_max, top_left, top_right = find_top(mask)
    bottom_heights, bottom_max, bottom_left, bottom_right = find_bottom(mask, height_of_dia)
    p_bottom_max = (bottom_max, bottom_heights[bottom_max])
    p_bottom_max_top = (bottom_max, top_heights[bottom_max])
    h_bottom_max = -(top_heights[bottom_max]- bottom_heights[bottom_max])
    p_top_max = (top_max, top_heights[top_max])
    p_top_max_bottom = (top_max, bottom_heights[top_max])
    h_top_max = -(top_heights[top_max]-bottom_heights[top_max])
    
    return {"P bottom max":p_bottom_max, "P over bottom max":p_bottom_max_top, "H bottom max":h_bottom_max, 
            "P top max":p_top_max, "P under top max":p_top_max_bottom, "H top max":h_top_max, "Bottom heights":bottom_heights, 
            "Top heights":top_heights, "COM":com, 'Left dia edge':bottom_left, 'Right dia edge':bottom_right, 
            'Left dia edge top':top_left, 'Right dia edge top':top_right, 'Width':width, 'X of height':x_of_height}

def max_filter(dia, threshhold):
    '''Assign any colum the maximal valued pixel as dia position'''
    mask = dia.copy()
    mask[mask<threshhold]=0
    maxs = np.max(mask, axis=0)
    maxs = np.repeat(maxs.reshape(1,len(maxs)), len(maxs), axis=0)
    output = np.zeros_like(mask)
    output[(mask==maxs)*(mask>threshhold/2)]=1.0
    fails = np.sum(output, axis=0)>1
    if fails.any():
        print("At least one double max in diaphragm output")
        output[:,fails]=0.0
    return output

def weights_maxfiltered(mask, alpha=1.0):
    '''Assign weights to the columns of the mask, with the maximal valued pixel as dia position'''
    dim = mask.shape[0]
    occ = np.sum(mask, axis=0)>0
    middle = int(ndimage.center_of_mass(occ)[0])
    x = (np.arange(dim) - middle)[occ]
    x = x/np.max(x)
    weights = (1+alpha*x**2)
    return weights

def find_BB_hole(dim, xs, analysis_frame):
    '''Find the hole in lung (where the backbone is visible), where the diaphragm is not visible'''
    if len(analysis_frame)!=2:
        return False
    else:
        pos=np.array([analysis_frame[0]['Left dia edge top'], analysis_frame[0]['Right dia edge top'], 
                      analysis_frame[1]['Left dia edge top'], analysis_frame[1]['Right dia edge top']])
        pos.sort()
        left=pos[1]-3
        right=pos[2]+3
        occ = np.empty(dim)
        occ[:] = True
        occ[xs] = False
        labels, N_labels = ndimage.label(occ)
        allowed_labels = []
        for i in range(1,N_labels+1):
            com = ndimage.center_of_mass(labels==i)[0]
            if left < com and com < right and np.sum(labels==i)>20:
                allowed_labels.append(i)
        if len(allowed_labels)==1:
            return np.argwhere(labels==allowed_labels[0]).flatten()
        elif len(allowed_labels)==0:
            return False
        else:
            print("Backbone hole search to many possible labels between {} and {}".format(left, right))
            print(labels[left:right])
            return False


def low_conn_comps(mask):
    '''Find the largest connected component in a binary mask, but only if it is significantly larger than the second largest'''
    label_mask, N_labels = ndimage.label(mask)
    sizes = ndimage.sum(mask, label_mask, range(N_labels+1))
    heights = ndimage.sum(np.indices(mask.shape)[0], label_mask, range(N_labels+1))/np.where(sizes>0, sizes, np.ones_like(sizes))
    if N_labels == 0:
        mask_n = (label_mask==1)
    else:
        if np.sort(sizes)[-2] < sizes.max()/6:
            mask_n = (label_mask==sizes.argmax())
        else:
            heights[sizes<np.sort(sizes)[-2]] = 0
            mask_n = (label_mask==heights.argmax())
    mask_n = ndimage.morphology.binary_fill_holes(mask_n)
    return mask_n


def fit_dia(dia, analysis_frame, m_type, last_dia):
    '''Fit the diaphragm to the mask and the analysis_frame'''
    dim = dia.shape[-1]
    threshhold = 0.05
    dias = []
    for side in range(dia.shape[0]):
        if m_type=='Coronar':
            if CUPY:
                kernel = cp.ones((25,70))
                conn_comp = conn_comps(cp.asnumpy(convolve_gpu(cp.asarray((dia[side]>threshhold)), kernel)*1.0>0))
            else:
                kernel = np.ones((25,70))
                conn_comp = conn_comps(ndimage.convolve((dia[side]>threshhold), kernel)*1.0>0)

        elif m_type=='Sagittal': 
            if CUPY:
                kernel = cp.ones((40,100))
                conn_comp = conn_comps(cp.asnumpy(convolve_gpu(cp.asarray((dia[side]>threshhold)), kernel)*1.0>0))
            else:
                kernel = np.ones((40,100))
                conn_comp = conn_comps(ndimage.convolve((dia[side]>threshhold), kernel)*1.0>0)

        ldm = np.zeros((dim, dim))
        valid=(~np.isnan(last_dia[side,0]))*(~np.isnan(last_dia[side,1]))
        ldm[last_dia[side,1,valid].astype(int), last_dia[side,0,valid].astype(int)] = 1
        ldm = ldm + np.roll(ldm, -4, axis=1) + np.roll(ldm, 4, axis=1)
        ldm = ldm + np.roll(ldm, 1, axis=0) + np.roll(ldm, 2, axis=0) + np.roll(ldm, 3, axis=0) + np.roll(ldm, 4, axis=0) + np.roll(ldm, 5, axis=0)
        ldm = ldm + np.roll(ldm, -6, axis=0)
        ldm=(ldm>0)
        ldm[:,ldm.sum(axis=0)==0]=True
            
        mask = max_filter(dia[side]*conn_comp*ldm,threshhold)
        points = np.argwhere(mask>threshhold/2)
        xs_full=np.empty(dim)
        xs_full[:]=np.nan
        ys_full=np.empty(dim)
        ys_full[:]=np.nan
        if len(points)>0:
            points = points[points[:,1].argsort()]
            xs = [p[1] for p in points]
            ys = [p[0] for p in points]
            xs_filled = np.arange(np.min(xs), np.max(xs)+1, 1, int)
            
            xs_full[xs_filled] = xs_filled
            xs_BB_hole = find_BB_hole(dim, xs, analysis_frame)
            ys_full = np.interp(xs_full, xs, ys)
            ys_full[xs_BB_hole] = np.nan
        dias.append(np.array([xs_full, ys_full]))
    return np.array(dias)


def analyze_batch(segm, dias, m_type, smooth=True):
    '''Analyze the batch of segmentations and diaphragm predictions and return the results'''
    dim=segm.shape[-1]
    if smooth:
        segm_sm = smooth_segmentation(segm[:,1:])
    else:
        segm_sm = segm[:,1:].numpy()
    masks = []
    analysis = []
    dias_r = []
    if m_type=='Coronar':
        last_dia = np.empty((2,2,dim))
    else:
        last_dia = np.empty((1,2,dim))
    last_dia[:,:,:] = np.nan
    for idx, frame in enumerate(segm_sm):
        full_masks=[]
        edge_masks=[]
        analysis_frame=[]
        for side in range(frame.shape[0]):
            mask_full = conn_comps(frame[side])
            mask_full, mask_edge = mask_devision(1.0*mask_full)
            analysis_side = analyze_mask(mask_full)
            full_masks.append(mask_full)
            edge_masks.append(mask_edge)
            analysis_frame.append(analysis_side)
        masks.append(full_masks+edge_masks)
        analysis.append(analysis_frame)
        last_dia = fit_dia(dias[idx].numpy(), analysis_frame, m_type, last_dia)
        dias_r.append(last_dia.copy())
    if smooth:
        dias_sm = smooth_dias(np.array(dias_r))
    else:
        dias_sm = np.array(dias_r)
    
    return np.array(masks), analysis, dias_sm


def analysis_data_only(unet, image_path, analysis_func, m_type, dim=298, eval_batchsize = 10, device='cpu'):
    '''Analyzes the data from the image_path using the unet and the analysis_func, returning the results'''
    input_data = getMrtPictures(dim, image_path)
    segmentation, dias = comp_segmentation(unet, input_data, eval_batchsize = eval_batchsize, device=device)
    masks, analysis, dias = analyze_batch(segmentation, dias, m_type)
    dataset = analysis_func(masks, analysis, dias) + (m_type,)
    return {'Images':input_data, 'Segmentation':segmentation, 'Masks':(masks>0), 'Dias':dias, 'Dataset':dataset, 'Analysis':analysis}
