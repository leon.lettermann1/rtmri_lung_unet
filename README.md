# U-Net to segment lung area and diaphragm position in Real-Time MRI imaging

This repository provides the python code of custom defined functions (in src) and workflows (in scripts) used to obtain time resolved analysis of Real-Time MRI images showing lung and diaphragm of patients and healthy controls, and is related to an upcoming publication. While this is the code, an example dataset and intermediate and final results as well as the trained network weights are available on zenodo: https://doi.org/10.5281/zenodo.13483533

For questions, contact leonlettermann (at) gmail.com .

## Repository structure
The repository is organized in src, containing the codebase, and scripts, demonstrating the application separatly for coronar and sagittal MRI measurements. We provide and example dataset on zenodo, as well as the network weights trained on a larger dataset. To use the scripts with the example files, download the zenodo repository and enter its location under ZENODO_PATH in the scripts.

    ├── README.md               <- Git markdown file
    ├── LICENSE                 <- License file
    ├── environment             <- environment file to generate the necessary python environment with conda
    ├── src                     <- Folder containing code base.
    │   ├── Preprocessing.py    <- Functions for reading images and masks, data augmentation by distortion.
    │   ├── NeuralNetwork.py    <- Definition of the neural network, loss and trainings functions.
    │   ├── Postprocessing.py   <- Application of the neural network to datasets and analyzing resulting masks.
    │   └── Visualization.py    <- Graphical representation of the results, generating animations.
    │
    └── scripts                 <- Folder containing scripts to apply the pipeline to an example dataset.
        ├── Prepare_Trainingsdata_Coronar.py  <- Loads images and masks, distorts them and saves trainings data.
        ├── Train_Network_Coronar.py          <- Trains the network with prepared training data.
        ├── Analyse_Coronar.py                <- Apply the network with the trained weights to analyse a dataset.
        ├── Prepare_Trainingsdata_Sagittal.py <- Same as above, but for sagittal images (which differs 
        ├── Train_Network_Sagittal.py                in structure as not two lungs(left and right) but only 
        └── Analyse_Sagittal.py                      one side is visible and segmented in each measurement.