# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).

# %%
import numpy as np
import torch
import os
import pickle
import sys


sys.path.append(os.path.abspath("./../src"))
from NeuralNetwork import UNET
from Postprocessing import analysis_data_only
from Visualization import do_per_frame, af_width_mean, af_polynomial_movement, af_polynomial_movement_vd, af_dia_movement_sag, fit_sin_to_plot, animation_from_data


ZENODO_PATH = './../../zenodo/'

# %%
cm_per_pxl = 0.175
secs_per_frame = 1/20
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Using CUDA")
elif torch.backends.mps.is_available():
    device = torch.device("mps")
    print("Using MPS")
else:
    device = torch.device("cpu")
    print("Using CPU")
# %%
# load a trained net and move it to the GPU for use
unet = UNET(1,2, N_dias=1, Exp_Padding=0)

# Load network state:
# Load weights trained on the example dataset. This is only for dempnstration, and does not necessary yield good results.
#unet.load_state_dict(torch.load(os.path.join(ZENODO_PATH, 'Trained_Weights/SagittalExample.pth'), map_location=device, weights_only=False))
# Load weights trained on the full dataset. This is only for dempnstration, and does not necessary yield good results.
unet.load_state_dict(torch.load(os.path.join(ZENODO_PATH, 'Trained_Weights/SagittalFull.pth'), map_location=device, weights_only=False))
unet.to(device)
print("UNET loaded successfully")

# %%
textsize = 31
legendsize = 24
def full_analysis(masks, analysis, dias):
    # polynomial movement analysis of diaphragm
    data_and_polys, plot_inst1 = do_per_frame(len(analysis), af_polynomial_movement, (analysis, dias))
    data, polys, offsets, xs_polys, ys_polys = np.array([d['data'] for d in data_and_polys]), np.array([d['coeffs'][0:1] for d in data_and_polys]), np.array([d['offset'][0:1] for d in data_and_polys]), [d['xs'][0:1] for d in data_and_polys], [d['ys'][0:1] for d in data_and_polys]
    data7 = data[:,2:] # splitting of integral data for area plot

    # diaphragm movement:
    data2, plot_inst2 = do_per_frame(len(analysis), af_dia_movement_sag, (analysis, dias))
    other2 =  {'ylim':(-0.5,9.5), 'xlabel': ("Time, s", {'fontsize':textsize}), 'ylabel': ("Diaphragmatic Motion., cm", {'fontsize':textsize, 'labelpad':25}), 
              'manual colors':['C2', 'C2'], 'manual linestyles':['-', '--'], 'manual linewidth':[6, 6]}
    data2 = np.array(data2)
    data2 = cm_per_pxl*data2
    data2 = data2 - np.nanmin(data2, axis=0)
    plot2 = ((data2, {}, {}), other2)
    def update_plot_inst2(p):
        for i in range(len(p['lines'])):
            p['lines'][i][1]['c'] = ['C2', 'C2', 'C8', 'C3'][i]
        return p
    plot_inst2 = [update_plot_inst2(p) for p in plot_inst2]

    # polynomial analysis of movement of thorax wall front:
    data5, plot_inst5 = do_per_frame(len(analysis), af_polynomial_movement_vd, (analysis, dias, 0))
    data5 = np.array(data5) 
    data5 = data5 - np.nanmean(data5, axis=0)

    # polynomial analysis of movement of thorax wall back:
    data6, plot_inst6 = do_per_frame(len(analysis), af_polynomial_movement_vd, (analysis, dias, 1))
    data6 = np.array(data6)  
    data6 = data6 - np.nanmean(data6, axis=0)

    #width mean:
    top_height = np.array([int(np.max(np.array([a[0]['P top max'][1] for a in analysis])))])
    bottom_height = np.array([int(np.min([np.nanmin(dias[frame,0,1][np.arange(analysis[frame][0]['Left dia edge top'], analysis[frame][0]['Right dia edge top'])])-7 for frame in range(len(analysis))]))])
    distance = bottom_height-top_height
    data3, plot_inst3 = do_per_frame(len(analysis), af_width_mean, (analysis, dias, top_height, distance))
    data3 = np.array(data3)
    other3 = {'xlabel': ("Time, s", {'fontsize':textsize}), 'ylabel': ("Chest Wall Motion, cm", {'fontsize':textsize}), 
              'ylim':(12, 20), 'manual colors':['C1', 'C1'], 'manual linestyles':['-', '--'], 'manual linewidth':[6, 6]}
    plot3 = ((cm_per_pxl*data3, {}, {}), other3)
    def update_plot_inst3(p):
        for i in range(len(p['lines'])):
            p['lines'][i][1]['c'] = 'C1'
        return p
    plot_inst3 = [update_plot_inst3(p) for p in plot_inst3]

    #Area and Integral
    areas = np.sum(masks[:,0:1], axis=(2,3))
    exsp_indcs = areas.argsort(axis=0)[:5]
    area_exsp = np.mean(areas[exsp_indcs, np.arange(1)], axis=0)
    int_Zf_exsp = np.nanmean(data7[exsp_indcs, np.arange(1)], axis=0)
    int_Vent_exsp = np.nanmean(data5[exsp_indcs, np.array([2])], axis=0)
    int_Dors_exsp = np.nanmean(data6[exsp_indcs, np.array([2])], axis=0)
    
    data8 = np.concatenate((areas, data7-int_Zf_exsp+area_exsp, -(data5[:,2:]-int_Vent_exsp)+data6[:,2:]-int_Dors_exsp+area_exsp), axis=1)*cm_per_pxl**2
    other5 = {'label':(['Total', 'Diaphragm', 'Chest Wall'],{'ncol':3,'fontsize':legendsize, 'handlelength':2.5, 'loc':'upper right'}), 
              'xlabel': ("Time, s", {'fontsize':textsize}), 'ylabel': ("Total Lung Area, cm²", {'fontsize':textsize}),
              'ylim':(70,300), 'manual colors':['C0', 'C2', 'C1'], 'manual linestyles':['-', '-', '-'], 'manual linewidth':[6, 6, 6]}
    plot5 = ((data8, {}, {}), other5)
    

    #fit sin
    params = fit_sin_to_plot(data5[:,:2].sum(axis=1))
    plot_inst = [{'points':plot_inst1[i]['points']+plot_inst3[i]['points'], 'lines':plot_inst1[i]['lines']+plot_inst3[i]['lines']} for i in range(len(analysis))]
    return (2, 1), [plot2, plot3, plot5], plot_inst, plot_inst2, {'Sinusfit':params, 'Polys':polys, 'Offsets':offsets, 'Xs_Polys': xs_polys, 'Ys_Polys':ys_polys}

# %%
list_patients = ["Example"]
input_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Rawdata/Sagittal/')
output_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Analysis/Data/')

for pat in list_patients:
    for meas in ['sag_L', 'sag_R', 'sniff_L', 'sniff_R']:
        datadict = analysis_data_only(unet, os.path.join(input_path, pat+"_"+meas+"/"), full_analysis, 'Sagittal', eval_batchsize=100, device=device)
        outpath_pat = os.path.join(output_path, pat)
        if not os.path.isdir(outpath_pat):
            os.mkdir(outpath_pat)
        with open(outpath_pat+'/'+meas+'.pickle', 'wb') as fp:
            pickle.dump(datadict, fp, protocol=pickle.HIGHEST_PROTOCOL)

# %%
video_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Analysis/Videos/')
for pat in list_patients:
    for meas in ['sag_L', 'sag_R', 'sniff_L', 'sniff_R']:
        animation_from_data(output_path, pat, meas, video_path+pat+"_"+meas+".mp4")