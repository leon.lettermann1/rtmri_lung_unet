# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).

# %%
import numpy as np
import torch
import os
import sys

sys.path.append(os.path.abspath("./../src"))
from Preprocessing import getMrtData, genDistorsions, get_1Hot, plotMasksBunch

ZENODO_PATH = './../../zenodo/'

# %%
# Input WS
training_data_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Trainingsdata/Coronar/')
image_path_MRT = os.path.join(training_data_path, 'images/')
mask_MRT_L = os.path.join(training_data_path, 'Masks/masks_L/')
mask_MRT_R = os.path.join(training_data_path, 'Masks/masks_R/')
mask_MRT_Dia = os.path.join(training_data_path, 'Masks/masks_Dia/')
dim = 298
X_MRT, y_MRT = getMrtData(dim, image_path_MRT, [mask_MRT_L, mask_MRT_R, mask_MRT_Dia])

# %%
N_pics_validation = 2 # Number of base images (without distortions) in validation set (Because we only provide one probands data, this is only for illustration purposes)
N_distortions = 19 # Number of distorted images per image

# Spliting the imported images into the test and validation set, and computing (in both cases) N_distortions distortions of every image
N_train = len(X_MRT) - N_pics_validation # Number of base images in training set, hence total number of images in training set is N_train * (1+N_distortions)

perm=np.random.permutation(range(len(X_MRT)))
X_MRT, y_MRT  =  [X_MRT[i] for i in perm], [y_MRT[i] for i in perm]

print("Distorting training images:")
X_train, y_train = genDistorsions(dim, X_MRT[0:N_train], y_MRT[0:N_train], N_distortions)
print("Distorting validation images:")
X_valid, y_valid = genDistorsions(dim, X_MRT[N_train:], y_MRT[N_train:], N_distortions)

input_train = X_train.view(len(X_train),1,dim,dim)
target_train = (y_train[:,0]+y_train[:,1]*2).long().view(len(y_train),dim,dim)
have_Dia_mask = y_train[:,2].sum(axis=(1,2))!=0
dias_train = np.zeros_like(target_train, dtype=bool)[:,None]
dias_train[have_Dia_mask] = get_1Hot(y_train[have_Dia_mask][:,2:3], use_conn_comps=False)
dias_train[~have_Dia_mask] = get_1Hot(y_train[~have_Dia_mask][:,:2])
dias_train = torch.tensor(dias_train)

input_valid = X_valid.view(len(X_valid),1,dim,dim)
target_valid = (y_valid[:,0]+y_valid[:,1]*2).long().view(len(y_valid),dim,dim)
have_Dia_mask = y_valid[:,2].sum(axis=(1,2))!=0
dias_valid = np.zeros_like(target_valid, dtype=bool)[:,None]
dias_valid[have_Dia_mask] = get_1Hot(y_valid[have_Dia_mask][:,2:3], use_conn_comps=False)
dias_valid[~have_Dia_mask] = get_1Hot(y_valid[~have_Dia_mask][:,:2])
dias_valid = torch.tensor(dias_valid)

# %%
# Quick visualization of the training data
indices = np.random.permutation(range(len(input_train)))[0:12]
plotMasksBunch(input_train[indices[::2]][:,0]+0.5*target_train[indices[::2]]+1*dias_train[indices[::2]][:,0], 
               input_train[indices[1::2]][:,0]+0.5*target_train[indices[1::2]]+1*dias_train[indices[1::2]][:,0], 2)

# %%
# Quick visualization of the validation data
indices = np.random.permutation(range(len(input_valid)))[0:12]
plotMasksBunch(input_valid[indices[::2]][:,0]+0.5*target_valid[indices[::2]]+1*dias_valid[indices[::2]][:,0], 
               input_valid[indices[1::2]][:,0]+0.5*target_valid[indices[1::2]]+1*dias_valid[indices[1::2]][:,0], 2)

# %%
tensor_paths = ["input_train.pt","input_valid.pt","target_train.pt","target_valid.pt","dias_train.pt","dias_valid.pt"]
tensordata_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Tensordata/Coronar/')
tensor_paths = [tensordata_path + t for t in tensor_paths]

# %%
torch.save(input_train, tensor_paths[0])
torch.save(input_valid, tensor_paths[1])
torch.save(target_train, tensor_paths[2])
torch.save(target_valid, tensor_paths[3])
torch.save(dias_train, tensor_paths[4])
torch.save(dias_valid, tensor_paths[5])