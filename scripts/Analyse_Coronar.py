# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).

# %%
import numpy as np
import torch
import os
import pickle
import sys


sys.path.append(os.path.abspath("./../src"))
from NeuralNetwork import UNET
from Postprocessing import analysis_data_only
from Visualization import do_per_frame, af_width_mean, af_polynomial_movement, af_dia_movement_cor, fit_sin_to_plot, animation_from_data


ZENODO_PATH = './../../zenodo/'

# %%
cm_per_pxl = 0.15
secs_per_frame = 1/20
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Using CUDA")
elif torch.backends.mps.is_available():
    device = torch.device("mps")
    print("Using MPS")
else:
    device = torch.device("cpu")
    print("Using CPU")
# %%
# load a trained net and move it to the GPU for use
unet = UNET(1,3, N_dias=1, Exp_Padding=0)

# Load network state:
# Load weights trained on the example dataset. This is only for dempnstration, and does not necessary yield good results.
#unet.load_state_dict(torch.load(os.path.join(ZENODO_PATH, 'Trained_Weights/CoronarExample.pth'), map_location=device, weights_only=False))
# Load weights trained on the full dataset. This is only for dempnstration, and does not necessary yield good results.
unet.load_state_dict(torch.load(os.path.join(ZENODO_PATH, 'Trained_Weights/CoronarFull.pth'), map_location=device, weights_only=False))
unet.to(device)
print("UNET loaded successfully")

# %%
textsize = 31
legendsize = 24
def full_analysis(masks, analysis, dias):
    # polynomial movement analysis of diaphragm
    data_and_polys, plot_inst1 = do_per_frame(len(analysis), af_polynomial_movement, (analysis, dias))
    data, polys, offsets, xs_polys, ys_polys = np.array([d['data'] for d in data_and_polys]), np.array([d['coeffs'] for d in data_and_polys]), np.array([d['offset'] for d in data_and_polys]), [d['xs'] for d in data_and_polys], [d['ys'] for d in data_and_polys]
    data5 = data[:,[2,5]] # splitting of integral data for area plot

    # diaphragm movement:
    data2, plot_inst2 = do_per_frame(len(analysis), af_dia_movement_cor, (analysis, dias))
    other2 = {'label':(['Right', 'Left'], {'ncol':2,'fontsize':legendsize, 'handlelength':2.5, 'loc':'upper right'}), 
              'ylim':(-0.5,7.5), 'xlabel': ("Time, s", {'fontsize':textsize}), 'ylabel': ("Diaphragmatic Motion., cm", {'fontsize':textsize, 'labelpad':25}), 
              'manual colors':['C2', 'C2'], 'manual linestyles':['-', '--'], 'manual linewidth':[6, 6]}
    data2 = np.array(data2)[:,:2]
    data2 = cm_per_pxl*data2
    data2 = data2 - np.nanmin(data2, axis=0)
    plot2 = ((data2, {}, {}), other2)
    def update_plot_inst2(p):
        for i in range(len(p['lines'])):
            p['lines'][i][1]['c'] = ['C2', 'C2', 'C8', 'C3'][i]
        return p
    plot_inst2 = [update_plot_inst2(p) for p in plot_inst2]

    #width mean:
    top_height = np.array([int(np.max(np.array([a[0]['P top max'][1] for a in analysis]))), int(np.mean(np.array([a[1]['P top max'][1] for a in analysis])))])
    bottom_height = np.array([int(np.min([np.nanmin(dias[frame,0,1][np.arange(analysis[frame][side]['Left dia edge top'], analysis[frame][side]['Right dia edge top'])]) for frame in range(len(analysis))])) for side in range(len(analysis[0]))])
    distance = bottom_height-top_height
    data3, plot_inst3 = do_per_frame(len(analysis), af_width_mean, (analysis, dias, top_height, distance))
    data3 = np.array(data3)
    other3 = {'label':(['Right', 'Left'],{'ncol':2,'fontsize':legendsize, 'handlelength':2.5, 'loc':'upper right'}), 'xlabel': ("Time, s", {'fontsize':textsize}), 'ylabel': ("Chest Wall Motion, cm", {'fontsize':textsize}), 
              'ylim':(4, 12), 'manual colors':['C1', 'C1'], 'manual linestyles':['-', '--'], 'manual linewidth':[6, 6]}
    plot3 = ((cm_per_pxl*data3, {}, {}), other3)
    def update_plot_inst3(p):
        for i in range(len(p['lines'])):
            p['lines'][i][1]['c'] = 'C1'
        return p
    plot_inst3 = [update_plot_inst3(p) for p in plot_inst3]
    
    #Area and Integral
    areas = np.sum(masks[:,[0,1]], axis=(2,3))
    exsp_indcs = areas.argsort(axis=0)[:5]
    area_exsp = np.mean(areas[exsp_indcs, np.arange(2)], axis=0)
    int_exsp = np.nanmean(data5[exsp_indcs, np.arange(2)], axis=0)

    data5 = np.concatenate((areas, data5-int_exsp+area_exsp), axis=1)*cm_per_pxl**2
    data5 = data5[:,:2]
    other5 = {'label':(['Right', 'Left'],{'ncol':2,'fontsize':legendsize, 'handlelength':2.5, 'loc':'upper right'}), 'xlabel': ("Time, s", {'fontsize':textsize}), 'ylabel': ("Total Lung Area, cm²", {'fontsize':textsize}),
              'ylim':(66,265 ), 'manual colors':['C0', 'C0'], 'manual linestyles':['-', '--'], 'manual linewidth':[6, 6]}
    plot5 = ((data5, {}, {}), other5)

    #fit sin
    params = fit_sin_to_plot(data5[:,:2].sum(axis=1))

    plot_inst = [{'points':plot_inst1[i]['points']+plot_inst3[i]['points'], 'lines':plot_inst1[i]['lines']+plot_inst3[i]['lines']} for i in range(len(analysis))]
    return (2, 1), [plot2, plot3, plot5], plot_inst, plot_inst2, {'Sinusfit':params, 'Polys':polys, 'Offsets':offsets, 'Xs_Polys': xs_polys, 'Ys_Polys':ys_polys}

# %%
list_patients = ["Example"]
input_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Rawdata/Coronar/')
output_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Analysis/Data/')

for pat in list_patients:
    for meas in ['ant', 'mid', 'post']:
        datadict = analysis_data_only(unet, os.path.join(input_path, pat+"_"+meas+"/"), full_analysis, 'Coronar', eval_batchsize=100, device=device)
        outpath_pat = os.path.join(output_path, pat)
        if not os.path.isdir(outpath_pat):
            os.mkdir(outpath_pat)
        with open(outpath_pat+'/'+meas+'.pickle', 'wb') as fp:
            pickle.dump(datadict, fp, protocol=pickle.HIGHEST_PROTOCOL)

# %%
video_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Analysis/Videos/')
for pat in list_patients:
    animation_from_data(output_path, pat, "ant", video_path+pat+"_ant.mp4")
    animation_from_data(output_path, pat, "post", video_path+pat+"_post.mp4")
    animation_from_data(output_path, pat, "mid", video_path+pat+"_mid.mp4")