# Copyright (c) 2024. Leon Lettermann.
# All rights reserved. This software is protected by the BSD 3-Clause License (see LICENSE for more details).

# %%
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import os
import sys

sys.path.append(os.path.abspath("./../src"))
from Preprocessing import plotMasksBunch
from NeuralNetwork import UNET, dataset, learn_at_rate, comp_segmentation

ZENODO_PATH = './../../zenodo/'
# %%
# Creating the Unet, 3 outputs for nothing, left lung and right lung
unet = UNET(1,2,N_dias=1, Exp_Padding=1)
# Setting up the optimizer and loss function, sending the loss function and unet to the GPU if CUDA is supported
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Using CUDA")
elif torch.backends.mps.is_available():
    device = torch.device("mps")
    print("Using MPS")
else:
    device = torch.device("cpu")
    print("Using CPU")
CELoss = nn.CrossEntropyLoss().to(device)
BCELoss = nn.BCELoss().to(device)
def criterion(outputs, dias_outs, targets, dias_targets):
    return CELoss(outputs,targets), BCELoss(dias_outs, dias_targets)

# unet.load_state_dict(torch.load(os.path.join(ZENODO_PATH, 'Trained_Weights/SagittalFull.pth'), map_location=device))

# initializing the unet and sending it to the GPU
unet.init_weights()
unet.to(device)
print("UNET created sucessfully!")

# %%
tensor_paths = ["input_train.pt","input_valid.pt","target_train.pt","target_valid.pt","dias_train.pt","dias_valid.pt"]
tensordata_path = os.path.join(ZENODO_PATH, 'Example_Dataset/Tensordata/Sagittal/')
tensor_paths = [tensordata_path + t for t in tensor_paths]

# %%
dataset_train = dataset(tensor_paths[::2])
dataset_valid = dataset(tensor_paths[1::2])

# %%
losses = []

# Parameters used for full dataset on Nvidia GeForce GTX 1080 Ti:
# max_epochs, weight_decay,learning_rate,lr_decay,mini_batch_size = 100, 3e-1, 1e-3, 0.99, 30

# Parameters for example dataset:
mini_batch_size = 130 # Depends on the GPU memory 
max_epochs=500 # usually 70-100, depends on number of training samples
weight_decay= 1e-3
learning_rate = 2e-3
lr_decay = 0.996
mini_batch_size = 130 # Depends on the GPU memory 
unet.train()
optimizer = torch.optim.AdamW(unet.parameters(), lr=learning_rate, weight_decay=weight_decay)
scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, lr_decay)


termination = 0
for i in range(max_epochs): #np.arange(100)+100: #:
    print("Epoch: {}".format(i+1))
    loss = learn_at_rate(unet, dataset_train, dataset_valid, criterion, optimizer, mini_batch_size=mini_batch_size, weight_decay=weight_decay, device=device)
    losses.append(loss)
    if loss[0]<0.0078 and i>90: 
        termination = termination + 1
    else: 
        termination = 0
    if termination == 3: 
        break
    scheduler.step()

# %%
#Save a trained net
torch.save(unet.state_dict(), os.path.join(ZENODO_PATH, 'Trained_Weights/SagittalExample.pth'))

# %%
# Plot the losses
plt.figure(figsize=(10,7))
plt.plot(range(len(losses)), losses)
plt.yscale('log')
plt.legend(["Training Loss Segm", "Training Loss Dia", "Pixel Loss TrainSet", "Pixel Loss ValidSet", "Poly Loss TrainSet", "Poly Loss ValidSet"])
plt.grid()
#plt.savefig("./Outputs/A0_Loss.png")
#plt.close()

# %%
testset = dataset_train.inputs
test_input = testset
final_output, dias_output = comp_segmentation(unet, test_input, eval_batchsize=10, device=device)

# %%
start = 0
stepsize= 1
plotMasksBunch(1.0*test_input[start::stepsize,0].numpy(), 
               1.0*final_output[start::stepsize,1] +0.5*(dias_output[start::stepsize,0]>0.05).numpy(), 3)

# %%
